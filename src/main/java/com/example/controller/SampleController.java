package com.example.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Employee;

@RestController
public class SampleController
{
    @RequestMapping(value = "/getData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Employee getdata()
    {
//        HttpHeaders ht = new HttpHeaders();
//        ht.add("Content-Type", "application/json; charset=UTF-8");
        Employee e = new Employee();
        e.setId(123);
        e.setName("sanket");
        return e;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String get()
    {
        // Employee e= new Employee(123,"Sanket");
        return "home";
    }
}
